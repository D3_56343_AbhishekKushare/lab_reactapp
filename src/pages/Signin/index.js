import './index.css'
import { Link } from 'react-router-dom'
const Signin = () => {
  return (
    <div>
      <h1>Signin</h1>
      <div className="row">
        <div className="col"></div>
        <div className="col">
      <div className="form">
        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Email address
          </label>
          <input type="text" className="form-control" />
        </div>

        <div className="mb-3">
          <label htmlFor="" className="label-control">
            Password
          </label>
          <input type="password" className="form-control" />
        </div>
        <div className="mb-3">
        <div>
            No account yet? <Link to="/signup">Signup here</Link>
          </div>
          <button className="btn btn-outline-primary">Login</button>
        </div>
      </div>
    </div>
    <div className="col"></div>
      </div>
    </div>
  )
}

export default Signin